## Parallel Cooperative Memetic Co-evolution for VRPTW

**Miroslaw Blocho, Tomasz Jastrzab, Jakub Nalepa**

This repository contains the supplementary material accompanying the manuscript submitted to the Genetic and Evolutionary Computation Conference GECCO 2023. 
The supplementary material includes the pseudo-code of the developed Parallel Cooperative Co-evolutionary Memetic Algorithm (PCCMA) along with its detailed description. It also contains the visualization of the cooperation process and graphical presentation of some of the results.
